﻿
namespace AdminFrom
{
    partial class Admin
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Admin));
            this.pictureBoxWelcome = new System.Windows.Forms.PictureBox();
            this.Chooserlabel = new System.Windows.Forms.Label();
            this.pictureBoxHi = new System.Windows.Forms.PictureBox();
            this.pictureBoxGeo = new System.Windows.Forms.PictureBox();
            this.pictureBoxLi = new System.Windows.Forms.PictureBox();
            this.pictureBoxOther = new System.Windows.Forms.PictureBox();
            this.pictureBoxQuiz = new System.Windows.Forms.PictureBox();
            this.dataGridViewQuiz = new System.Windows.Forms.DataGridView();
            this.buttonHistory = new System.Windows.Forms.Button();
            this.buttonGeo = new System.Windows.Forms.Button();
            this.buttonLi = new System.Windows.Forms.Button();
            this.buttonOther = new System.Windows.Forms.Button();
            this.textBoxQuestion = new System.Windows.Forms.TextBox();
            this.textBoxAnswer1 = new System.Windows.Forms.TextBox();
            this.textBoxAnswer2 = new System.Windows.Forms.TextBox();
            this.textBoxAsnwer3 = new System.Windows.Forms.TextBox();
            this.textBoxAnswer4 = new System.Windows.Forms.TextBox();
            this.textBoxCorrect = new System.Windows.Forms.TextBox();
            this.labelQuestion = new System.Windows.Forms.Label();
            this.labelAnswer1 = new System.Windows.Forms.Label();
            this.labelAnswer2 = new System.Windows.Forms.Label();
            this.labelAnswer3 = new System.Windows.Forms.Label();
            this.labelAnswer4 = new System.Windows.Forms.Label();
            this.labelCorrect = new System.Windows.Forms.Label();
            this.buttonValid = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip5 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip6 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip7 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip8 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip9 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip10 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip11 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip12 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip13 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip14 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolTip15 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWelcome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGeo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQuiz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewQuiz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxWelcome
            // 
            this.pictureBoxWelcome.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxWelcome.Image")));
            this.pictureBoxWelcome.Location = new System.Drawing.Point(10, 9);
            this.pictureBoxWelcome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxWelcome.Name = "pictureBoxWelcome";
            this.pictureBoxWelcome.Size = new System.Drawing.Size(560, 128);
            this.pictureBoxWelcome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWelcome.TabIndex = 0;
            this.pictureBoxWelcome.TabStop = false;
            // 
            // Chooserlabel
            // 
            this.Chooserlabel.AutoSize = true;
            this.Chooserlabel.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Chooserlabel.Location = new System.Drawing.Point(10, 149);
            this.Chooserlabel.Name = "Chooserlabel";
            this.Chooserlabel.Size = new System.Drawing.Size(147, 20);
            this.Chooserlabel.TabIndex = 1;
            this.Chooserlabel.Text = "Choose a category";
            // 
            // pictureBoxHi
            // 
            this.pictureBoxHi.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxHi.Image")));
            this.pictureBoxHi.Location = new System.Drawing.Point(11, 182);
            this.pictureBoxHi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxHi.Name = "pictureBoxHi";
            this.pictureBoxHi.Size = new System.Drawing.Size(160, 50);
            this.pictureBoxHi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxHi.TabIndex = 2;
            this.pictureBoxHi.TabStop = false;
            // 
            // pictureBoxGeo
            // 
            this.pictureBoxGeo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxGeo.Image")));
            this.pictureBoxGeo.Location = new System.Drawing.Point(11, 244);
            this.pictureBoxGeo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxGeo.Name = "pictureBoxGeo";
            this.pictureBoxGeo.Size = new System.Drawing.Size(160, 50);
            this.pictureBoxGeo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxGeo.TabIndex = 3;
            this.pictureBoxGeo.TabStop = false;
            // 
            // pictureBoxLi
            // 
            this.pictureBoxLi.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLi.Image")));
            this.pictureBoxLi.Location = new System.Drawing.Point(11, 308);
            this.pictureBoxLi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxLi.Name = "pictureBoxLi";
            this.pictureBoxLi.Size = new System.Drawing.Size(160, 50);
            this.pictureBoxLi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLi.TabIndex = 3;
            this.pictureBoxLi.TabStop = false;
            // 
            // pictureBoxOther
            // 
            this.pictureBoxOther.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxOther.Image")));
            this.pictureBoxOther.Location = new System.Drawing.Point(11, 370);
            this.pictureBoxOther.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxOther.Name = "pictureBoxOther";
            this.pictureBoxOther.Size = new System.Drawing.Size(160, 50);
            this.pictureBoxOther.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxOther.TabIndex = 3;
            this.pictureBoxOther.TabStop = false;
            // 
            // pictureBoxQuiz
            // 
            this.pictureBoxQuiz.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxQuiz.Image")));
            this.pictureBoxQuiz.Location = new System.Drawing.Point(576, 9);
            this.pictureBoxQuiz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxQuiz.Name = "pictureBoxQuiz";
            this.pictureBoxQuiz.Size = new System.Drawing.Size(684, 128);
            this.pictureBoxQuiz.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxQuiz.TabIndex = 4;
            this.pictureBoxQuiz.TabStop = false;
            // 
            // dataGridViewQuiz
            // 
            this.dataGridViewQuiz.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewQuiz.Location = new System.Drawing.Point(210, 182);
            this.dataGridViewQuiz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewQuiz.Name = "dataGridViewQuiz";
            this.dataGridViewQuiz.RowHeadersWidth = 51;
            this.dataGridViewQuiz.RowTemplate.Height = 29;
            this.dataGridViewQuiz.Size = new System.Drawing.Size(1050, 238);
            this.dataGridViewQuiz.TabIndex = 5;
            this.dataGridViewQuiz.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewQuiz_RowHeaderMouseClick);
            // 
            // buttonHistory
            // 
            this.buttonHistory.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonHistory.Font = new System.Drawing.Font("Viner Hand ITC", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonHistory.Location = new System.Drawing.Point(210, 146);
            this.buttonHistory.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonHistory.Name = "buttonHistory";
            this.buttonHistory.Size = new System.Drawing.Size(199, 30);
            this.buttonHistory.TabIndex = 6;
            this.buttonHistory.Text = "History";
            this.buttonHistory.UseVisualStyleBackColor = false;
            this.buttonHistory.Click += new System.EventHandler(this.buttonHistory_Click);
            // 
            // buttonGeo
            // 
            this.buttonGeo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonGeo.Font = new System.Drawing.Font("Viner Hand ITC", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonGeo.Location = new System.Drawing.Point(490, 146);
            this.buttonGeo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonGeo.Name = "buttonGeo";
            this.buttonGeo.Size = new System.Drawing.Size(199, 30);
            this.buttonGeo.TabIndex = 6;
            this.buttonGeo.Text = "Geography";
            this.buttonGeo.UseVisualStyleBackColor = false;
            this.buttonGeo.Click += new System.EventHandler(this.buttonGeo_Click);
            // 
            // buttonLi
            // 
            this.buttonLi.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonLi.Font = new System.Drawing.Font("Viner Hand ITC", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonLi.Location = new System.Drawing.Point(768, 147);
            this.buttonLi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonLi.Name = "buttonLi";
            this.buttonLi.Size = new System.Drawing.Size(199, 30);
            this.buttonLi.TabIndex = 6;
            this.buttonLi.Text = "Literature";
            this.buttonLi.UseVisualStyleBackColor = false;
            this.buttonLi.Click += new System.EventHandler(this.buttonLi_Click);
            // 
            // buttonOther
            // 
            this.buttonOther.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonOther.Font = new System.Drawing.Font("Viner Hand ITC", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonOther.Location = new System.Drawing.Point(1061, 147);
            this.buttonOther.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonOther.Name = "buttonOther";
            this.buttonOther.Size = new System.Drawing.Size(199, 30);
            this.buttonOther.TabIndex = 6;
            this.buttonOther.Text = "Other";
            this.buttonOther.UseVisualStyleBackColor = false;
            this.buttonOther.Click += new System.EventHandler(this.buttonOther_Click);
            // 
            // textBoxQuestion
            // 
            this.textBoxQuestion.Location = new System.Drawing.Point(210, 443);
            this.textBoxQuestion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxQuestion.Multiline = true;
            this.textBoxQuestion.Name = "textBoxQuestion";
            this.textBoxQuestion.Size = new System.Drawing.Size(134, 54);
            this.textBoxQuestion.TabIndex = 7;
            // 
            // textBoxAnswer1
            // 
            this.textBoxAnswer1.Location = new System.Drawing.Point(348, 443);
            this.textBoxAnswer1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxAnswer1.Multiline = true;
            this.textBoxAnswer1.Name = "textBoxAnswer1";
            this.textBoxAnswer1.Size = new System.Drawing.Size(134, 54);
            this.textBoxAnswer1.TabIndex = 7;
            // 
            // textBoxAnswer2
            // 
            this.textBoxAnswer2.Location = new System.Drawing.Point(486, 443);
            this.textBoxAnswer2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxAnswer2.Multiline = true;
            this.textBoxAnswer2.Name = "textBoxAnswer2";
            this.textBoxAnswer2.Size = new System.Drawing.Size(134, 54);
            this.textBoxAnswer2.TabIndex = 7;
            // 
            // textBoxAsnwer3
            // 
            this.textBoxAsnwer3.Location = new System.Drawing.Point(625, 443);
            this.textBoxAsnwer3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxAsnwer3.Multiline = true;
            this.textBoxAsnwer3.Name = "textBoxAsnwer3";
            this.textBoxAsnwer3.Size = new System.Drawing.Size(134, 54);
            this.textBoxAsnwer3.TabIndex = 7;
            // 
            // textBoxAnswer4
            // 
            this.textBoxAnswer4.Location = new System.Drawing.Point(763, 443);
            this.textBoxAnswer4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxAnswer4.Multiline = true;
            this.textBoxAnswer4.Name = "textBoxAnswer4";
            this.textBoxAnswer4.Size = new System.Drawing.Size(134, 54);
            this.textBoxAnswer4.TabIndex = 8;
            // 
            // textBoxCorrect
            // 
            this.textBoxCorrect.Location = new System.Drawing.Point(901, 443);
            this.textBoxCorrect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxCorrect.Multiline = true;
            this.textBoxCorrect.Name = "textBoxCorrect";
            this.textBoxCorrect.Size = new System.Drawing.Size(134, 54);
            this.textBoxCorrect.TabIndex = 9;
            // 
            // labelQuestion
            // 
            this.labelQuestion.AutoSize = true;
            this.labelQuestion.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelQuestion.Location = new System.Drawing.Point(235, 422);
            this.labelQuestion.Name = "labelQuestion";
            this.labelQuestion.Size = new System.Drawing.Size(78, 20);
            this.labelQuestion.TabIndex = 1;
            this.labelQuestion.Text = "Question";
            // 
            // labelAnswer1
            // 
            this.labelAnswer1.AutoSize = true;
            this.labelAnswer1.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelAnswer1.Location = new System.Drawing.Point(381, 422);
            this.labelAnswer1.Name = "labelAnswer1";
            this.labelAnswer1.Size = new System.Drawing.Size(64, 20);
            this.labelAnswer1.TabIndex = 1;
            this.labelAnswer1.Text = "Answer";
            // 
            // labelAnswer2
            // 
            this.labelAnswer2.AutoSize = true;
            this.labelAnswer2.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelAnswer2.Location = new System.Drawing.Point(514, 422);
            this.labelAnswer2.Name = "labelAnswer2";
            this.labelAnswer2.Size = new System.Drawing.Size(64, 20);
            this.labelAnswer2.TabIndex = 1;
            this.labelAnswer2.Text = "Answer";
            // 
            // labelAnswer3
            // 
            this.labelAnswer3.AutoSize = true;
            this.labelAnswer3.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelAnswer3.Location = new System.Drawing.Point(656, 422);
            this.labelAnswer3.Name = "labelAnswer3";
            this.labelAnswer3.Size = new System.Drawing.Size(64, 20);
            this.labelAnswer3.TabIndex = 10;
            this.labelAnswer3.Text = "Answer";
            // 
            // labelAnswer4
            // 
            this.labelAnswer4.AutoSize = true;
            this.labelAnswer4.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelAnswer4.Location = new System.Drawing.Point(794, 422);
            this.labelAnswer4.Name = "labelAnswer4";
            this.labelAnswer4.Size = new System.Drawing.Size(64, 20);
            this.labelAnswer4.TabIndex = 11;
            this.labelAnswer4.Text = "Answer";
            // 
            // labelCorrect
            // 
            this.labelCorrect.AutoSize = true;
            this.labelCorrect.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelCorrect.Location = new System.Drawing.Point(901, 422);
            this.labelCorrect.Name = "labelCorrect";
            this.labelCorrect.Size = new System.Drawing.Size(125, 20);
            this.labelCorrect.TabIndex = 12;
            this.labelCorrect.Text = "Correct Answer";
            // 
            // buttonValid
            // 
            this.buttonValid.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonValid.Font = new System.Drawing.Font("Viner Hand ITC", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonValid.Location = new System.Drawing.Point(396, 515);
            this.buttonValid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonValid.Name = "buttonValid";
            this.buttonValid.Size = new System.Drawing.Size(199, 30);
            this.buttonValid.TabIndex = 13;
            this.buttonValid.Text = "Validate";
            this.buttonValid.UseVisualStyleBackColor = false;
            this.buttonValid.Click += new System.EventHandler(this.buttonValid_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buttonDelete.Font = new System.Drawing.Font("Viner Hand ITC", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonDelete.Location = new System.Drawing.Point(666, 515);
            this.buttonDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(199, 30);
            this.buttonDelete.TabIndex = 14;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipTitle = "Validate";
            // 
            // toolTip2
            // 
            this.toolTip2.ToolTipTitle = "Delete";
            // 
            // toolTip3
            // 
            this.toolTip3.ToolTipTitle = "History";
            // 
            // toolTip4
            // 
            this.toolTip4.ToolTipTitle = "Geography";
            // 
            // toolTip5
            // 
            this.toolTip5.ToolTipTitle = "Literature";
            // 
            // toolTip6
            // 
            this.toolTip6.ToolTipTitle = "Other";
            // 
            // toolTip7
            // 
            this.toolTip7.ToolTipTitle = "Question list";
            // 
            // toolTip8
            // 
            this.toolTip8.ToolTipTitle = "Question";
            // 
            // toolTip9
            // 
            this.toolTip9.ToolTipTitle = "Answer1";
            // 
            // toolTip10
            // 
            this.toolTip10.ToolTipTitle = "Answer2";
            // 
            // toolTip11
            // 
            this.toolTip11.ToolTipTitle = "Answer3";
            // 
            // toolTip12
            // 
            this.toolTip12.ToolTipTitle = "Answer4";
            // 
            // toolTip13
            // 
            this.toolTip13.ToolTipTitle = "Correct Anser";
            // 
            // toolTip14
            // 
            this.toolTip14.ToolTipTitle = "Hello";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(178, 199);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(26, 33);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1294, 599);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonValid);
            this.Controls.Add(this.labelCorrect);
            this.Controls.Add(this.labelAnswer4);
            this.Controls.Add(this.labelAnswer3);
            this.Controls.Add(this.textBoxCorrect);
            this.Controls.Add(this.textBoxAnswer4);
            this.Controls.Add(this.textBoxAsnwer3);
            this.Controls.Add(this.textBoxAnswer2);
            this.Controls.Add(this.textBoxAnswer1);
            this.Controls.Add(this.textBoxQuestion);
            this.Controls.Add(this.buttonOther);
            this.Controls.Add(this.buttonLi);
            this.Controls.Add(this.buttonGeo);
            this.Controls.Add(this.buttonHistory);
            this.Controls.Add(this.dataGridViewQuiz);
            this.Controls.Add(this.pictureBoxQuiz);
            this.Controls.Add(this.pictureBoxOther);
            this.Controls.Add(this.pictureBoxLi);
            this.Controls.Add(this.pictureBoxGeo);
            this.Controls.Add(this.pictureBoxHi);
            this.Controls.Add(this.labelAnswer2);
            this.Controls.Add(this.labelAnswer1);
            this.Controls.Add(this.labelQuestion);
            this.Controls.Add(this.Chooserlabel);
            this.Controls.Add(this.pictureBoxWelcome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Admin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWelcome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGeo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQuiz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewQuiz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxWelcome;
        private System.Windows.Forms.Label Chooserlabel;
        private System.Windows.Forms.PictureBox pictureBoxHi;
        private System.Windows.Forms.PictureBox pictureBoxGeo;
        private System.Windows.Forms.PictureBox pictureBoxLi;
        private System.Windows.Forms.PictureBox pictureBoxOther;
        private System.Windows.Forms.PictureBox pictureBoxQuiz;
        private System.Windows.Forms.DataGridView dataGridViewQuiz;
        private System.Windows.Forms.Button buttonHistory;
        private System.Windows.Forms.Button buttonGeo;
        private System.Windows.Forms.Button buttonLi;
        private System.Windows.Forms.Button buttonOther;
        private System.Windows.Forms.TextBox textBoxQuestion;
        private System.Windows.Forms.TextBox textBoxAnswer1;
        private System.Windows.Forms.TextBox textBoxAnswer2;
        private System.Windows.Forms.TextBox textBoxAsnwer3;
        private System.Windows.Forms.TextBox textBoxAnswer4;
        private System.Windows.Forms.TextBox textBoxCorrect;
        private System.Windows.Forms.Label labelQuestion;
        private System.Windows.Forms.Label labelAnswer1;
        private System.Windows.Forms.Label labelAnswer2;
        private System.Windows.Forms.Label labelAnswer3;
        private System.Windows.Forms.Label labelAnswer4;
        private System.Windows.Forms.Label labelCorrect;
        private System.Windows.Forms.Button buttonValid;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
        private System.Windows.Forms.ToolTip toolTip5;
        private System.Windows.Forms.ToolTip toolTip6;
        private System.Windows.Forms.ToolTip toolTip7;
        private System.Windows.Forms.ToolTip toolTip8;
        private System.Windows.Forms.ToolTip toolTip9;
        private System.Windows.Forms.ToolTip toolTip10;
        private System.Windows.Forms.ToolTip toolTip11;
        private System.Windows.Forms.ToolTip toolTip12;
        private System.Windows.Forms.ToolTip toolTip13;
        private System.Windows.Forms.ToolTip toolTip14;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolTip toolTip15;
    }
}

